import React, { Component } from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';

import Store from '../src/Store';
import { Header, CryptoContainer } from '../src/components';

export default class CryptoTrackerScreen extends React.Component {
  static navigationOptions = {
    title: 'CryptoTracker',
  };

  render() {
    /* Go ahead and delete ExpoConfigView and replace it with your
     * content, we just wanted to give you a quick view of your config */
    return  (
      <Provider store={Store}>
        <View>
          <Header />
          <CryptoContainer />
        </View>
      </Provider>
    );
  }
}
