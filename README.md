# Setup

React Native dev env setup:
https://expo.io/learn

Cryptocurrency App React native + Redux explain:
https://medium.com/react-native-training/bitcoin-ripple-ethereum-price-checker-with-react-native-redux-e9d076037092


```bash
Install Xcode
npm install expo-cli --global
npm install
expo start
```

# Example App
![picture](assets/images/image1.png)
![picture](assets/images/image2.png)
![picture](assets/images/image3.png)
![picture](assets/images/image4.png)
